import React, { Component } from 'react';
import { 
   View, 
   Text, 
   Button, 
   TextInput, 
   StyleSheet 
} from 'react-native';

//import userProfile from './resource/screens/profileUSer/userProfile.js'

class Login extends Component {
   // constructor(props){
   //    super(props){
   //       this.navigate= this.navigate.bind(this);
   //    }
   // }
  static navigationOptions = {
      title: 'User Login',
      header: null,
   };
  
   render() {
      return (
         <View style = {styles.container}>
            <View style = {styles.logoContainer}>
               <Text style={{color:'red'}}>Logo Goes Here</Text>
            </View>

         <View style = {styles.inputContainer}>
               
               <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Employee ID"
               placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               //onChangeText = {this.handleEmail}
               />
            
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Password"
               placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               //onChangeText = {this.handlePassword}
            />
            
            <Button title="Log In"
               style = {styles.submitButton}
               onPress = {() => 
                           {
                              //createAppContainer(AppNavigator);
                              this.props.navigation.navigate('userProfile')
                           }
                        }/>
               
        </View>
        <View style={{flex:1}}>

        </View>    
         </View>
      );
   }
}

// 
export default Login;


const styles = StyleSheet.create({
   container: {
      paddingTop: 2,
      justifyContent:'space-between',
      flex:1,
      backgroundColor:'#354766',
      margin:2
   },
   logoContainer:{
      flex:1,
     // color:'white',
     alignSelf:'center',
     backgroundColor:'#354766'
   },
   inputContainer:{
      flex:3,
      margin:20,
      justifyContent:'center',
      paddingTop: 8,
      backgroundColor:'white',
      paddingRight: 16,
      paddingBottom: 8,
      paddingLeft: 16,
      borderRadius: 6
   },
   input: {
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1
   },
   submitButton: {
      backgroundColor: '#7a42f4',
      padding: 16,
      borderRadius:8,
      height: 40,
      borderWidth:8,
      
      
   },
   submitButtonText:{
      color: 'black',
      alignSelf:'center',
      //backgroundColor:'white',
      borderWidth:2
      //text:'bold',
      //aspectRatio:'2'
   }
});